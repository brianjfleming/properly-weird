module.exports = {
	all: {
		options: {
			precision: 2,
			sourceMap: true
		},
		files: {
			'css/display/master.css': 'css/display/scss/master.scss',
			'css/display/homepage.css': 'css/display/scss/homepage.scss',
			'css/display/internal.css': 'css/display/scss/internal.scss'
        }
	}
};
module.exports = {
	dist: {
		options: {
			processors: [
				require('autoprefixer')({browsers: 'last 2 versions'})
			]
		},
		files: { 
			'css/display/master.css': [ 'css/display/master.css' ],
			'css/display/homepage.css': [ 'css/display/homepage.css' ],
			'css/display/internal.css': [ 'css/display/internal.css' ]
		}
	}
};
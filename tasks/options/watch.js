module.exports = {
	livereload: {
		files: ['css/display/*.css'],
		options: {
			livereload: true
		}
	},
	css: {
		files: ['css/display/scss/**/*.scss'],
		tasks: ['css'],
		options: {
			debounceDelay: 500
		}
	}
};

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Classes_model extends CI_Model {
    
    public $primary_table;
    public $path;
	
    
	// ----------------------------------- //
	// Initialize Our Primary Table
	// ----------------------------------- //
	public function initialize($table,$path) {
		$this->primary_table = $table;	
        $this->path = $path;
	}
    
    public function renderTable() {
        $rstr = '';
        
        $rstr .= '<table class="fdcms-table sortable" width="100%" cellpadding="0" cellspacing="0">';
        $rstr .= $this->renderTableRows();
        
        $rstr .= '<tr>';
        $rstr .= '<td colspan="2">';
        $rstr .= '<img src="/images/app/core/arrow_ltr.png"> <a href="javascript: void(0);" class="check-all edit-link">Check</a> / <a href="javascript: void(0);" class="uncheck-all delete-link">Uncheck</a> All';
        $rstr .= '<a href="javascript:void(0);" class="delete delete-selected" data-post="'.$this->path.'/delete">Delete Selected</a>';
        $rstr .= '</td>';
        $rstr .= '</tr>';
        
        $rstr .= '</table>';
        
        $rstr .= '
            <script>
            var fixHelper = function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            };                
            $(".sortable tbody").sortable({
                helper: fixHelper,
                update: function(event, ui) {
                    var newOrder = $(this).sortable(\'toArray\').toString();
                    $.post(\'/admin/classes/all/order\', {order:newOrder});
                }
            }).disableSelection();
            </script>
        ';
        
        return $rstr;
    }

	// ----------------------------------- //
	// Row output for pages table
	// ----------------------------------- //
    public function renderTableRows() {
        $rstr = '';
        
        $this->db->from($this->primary_table);
        $this->db->order_by("class_order", "asc");
        $query = $this->db->get();
        $count = $query->num_rows();
        $result = $query->result();
        
        if($count > 0) {
        
        foreach($result as $rs) {
            
            $class = ' class="native"';  
            
            $rstr .= '<tr id="'.$rs->class_id.'">';
            
            // delete checkbox
            $rstr .= '<td width="20"'.$class.' valign="top">';
            $rstr .= '<input type="checkbox" name="'.$rs->class_id.'" value="'.$rs->class_id.'" id="box_'.$rs->class_id.'" class="del-box" data-id="'.$rs->class_id.'" data-post="'.$this->path.'/delete/'.$rs->class_id.'">';
            $rstr .= '</td>';
            
            $rstr .= '<td'.$class.'>';
            $rstr .= '<div class="options-hover">';
            $rstr .= '<img src="'.$rs->class_image.'" width="100" style="float: right; margin: 8px;">';
            $rstr .= $rs->class_name;
            $rstr .= '<span class="row-options">';
            $rstr .= '<a href="'.$this->path.'/edit/'.$rs->class_id.'" class="edit-link">Edit</a>';
            $rstr .= ' | <a href="javascript:void(0)" class="delete-link delete-item" data-id="'.$rs->class_id.'" data-post="'.$this->path.'/delete/'.$rs->class_id.'">Delete</a>';
            $rstr .= '</span>';
            $rstr .= '</div>';
            $rstr .= '</td>';
            
            $rstr .= '</tr>';  
        }
        
        } else {
            $rstr .= '<tr><td>';
            $rstr .= '<div class="form-row"><div class="input-wrapper warning-text"><img src="/images/app/icons/warning.png" class="warning-icon"> <i>You haven\'t created any Classes to your website yet. Start by clicking "Add New Class" on the right.</b></i><img src="/images/app/icons/help.png" class="help-icon help" data-subject="add-menus"></div></div>';   
            $rstr .= '</td></tr>';
        }
        
        return $rstr;
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function create() {
        
        $data = array(
            'class_id' => '0',
            'class_name' => '',
            'class_desc' => '',
            'class_image' => ''
        );
        
        $rstr = '';
        $rstr .= '<div class="content-container">';
        $rstr .= $this->load->view('appForms/classEditForm', $data, true);
        $rstr .= '</div>';
        
        return $rstr;   
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function edit() {
        $id = $this->uri->segment(5);
        
        $SQL = "SELECT * FROM ".$this->primary_table." WHERE class_id = '".$id."' LIMIT 1";
        $query = $this->db->query($SQL);
        $row = $query->row();
        
        $data = array(
            'class_id' => $row->class_id,
            'class_name' => $row->class_name,
            'class_desc' => $row->class_desc,
            'class_image' => $row->class_image
        );
        
        $rstr = '';
        $rstr .= '<div class="content-container">';
        $rstr .= $this->load->view('appForms/classEditForm', $data, true);
        $rstr .= '</div>';
        
        return $rstr;   
    }

	// ----------------------------------- //
	// Saves and Creates pages
	// ----------------------------------- //
    public function save($image) {
        $id = $this->uri->segment(5);
        
        if($image == '') {
			$image = $this->input->post('class_image_current');	
		} else {
            $this->core_model->deleteFile($this->input->post('class_image_current'));   
        }
        
        $data = array(
            'class_id' => $this->input->post('class_id'),
            'class_name' => $this->input->post('class_name'),
            'class_desc' => $this->input->post('class_desc'),
            'class_image' => $image
        );
        
        
        if($id == 0) {          
            $this->db->insert($this->primary_table,$data);
			$return["id"] = $this->db->insert_id();
            $return["message"] = "The New Class Listing [ ".$data["class_name"]." ] was created successfully.";
        } else {
            // Saving an existing Page    
            $this->db->where('class_id',$id);
			$this->db->update($this->primary_table,$data); 
            $return["id"] = $id;
            $return["message"] = "Your changes have been saved.";
        }
        
        return $return;
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function getName($id) {
        $SQL = "SELECT * FROM ".$this->primary_table." WHERE class_id = '".$id."'";
        $query = $this->db->query($SQL);
        $row = $query->row();
        
        return $row->class_name;
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function editToolbox($id) {
        
        $toolboxArray = array();
		
        return $toolboxArray;
    }
    
    
    
    
}

?>
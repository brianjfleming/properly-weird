<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faqs_model extends CI_Model {
    
    public $primary_table;
    public $path;
	
    
	// ----------------------------------- //
	// Initialize Our Primary Table
	// ----------------------------------- //
	public function initialize($table,$path) {
		$this->primary_table = $table;	
        $this->path = $path;
	}
    
    public function renderTable() {
        $rstr = '';
        
        $rstr .= '<table class="fdcms-table sortable" width="100%" cellpadding="0" cellspacing="0">';
        $rstr .= $this->renderTableRows();
        
        $rstr .= '<tr>';
        $rstr .= '<td colspan="2">';
        $rstr .= '<img src="/images/app/core/arrow_ltr.png"> <a href="javascript: void(0);" class="check-all edit-link">Check</a> / <a href="javascript: void(0);" class="uncheck-all delete-link">Uncheck</a> All';
        $rstr .= '<a href="javascript:void(0);" class="delete delete-selected" data-post="'.$this->path.'/delete">Delete Selected</a>';
        $rstr .= '</td>';
        $rstr .= '</tr>';
        
        $rstr .= '</table>';
        
        $rstr .= '
            <script>
            var fixHelper = function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            };                
            $(".sortable tbody").sortable({
                helper: fixHelper,
                update: function(event, ui) {
                    var newOrder = $(this).sortable(\'toArray\').toString();
                    $.post(\'/admin/faqs/all/order\', {order:newOrder});
                }
            }).disableSelection();
            </script>
        ';
        
        return $rstr;
    }

	// ----------------------------------- //
	// Row output for pages table
	// ----------------------------------- //
    public function renderTableRows() {
        $rstr = '';
        
        $this->db->from($this->primary_table);
        $this->db->order_by("faq_order", "asc");
        $query = $this->db->get();
        $count = $query->num_rows();
        $result = $query->result();
        
        if($count > 0) {
        
        foreach($result as $rs) {
            
            $class = ' class="native"';  
            
            $rstr .= '<tr id="'.$rs->faq_id.'">';
            
            // delete checkbox
            $rstr .= '<td width="20"'.$class.' valign="top">';
            $rstr .= '<input type="checkbox" name="'.$rs->faq_id.'" value="'.$rs->faq_id.'" id="box_'.$rs->faq_id.'" class="del-box" data-id="'.$rs->faq_id.'" data-post="'.$this->path.'/delete/'.$rs->faq_id.'">';
            $rstr .= '</td>';
            
            $rstr .= '<td'.$class.'>';
            $rstr .= '<div class="options-hover">';
            $rstr .= '<b>'.$rs->faq_question.'</b>';
            $rstr .= '<br><i>'.$rs->faq_answer.'</i>';
            $rstr .= '<span class="row-options">';
            $rstr .= '<a href="'.$this->path.'/edit/'.$rs->faq_id.'" class="edit-link">Edit</a>';
            $rstr .= ' | <a href="javascript:void(0)" class="delete-link delete-item" data-id="'.$rs->faq_id.'" data-post="'.$this->path.'/delete/'.$rs->faq_id.'">Delete</a>';
            $rstr .= '</span>';
            $rstr .= '</div>';
            $rstr .= '</td>';
            
            $rstr .= '</tr>';  
        }
        
        } else {
            $rstr .= '<tr><td>';
            $rstr .= '<div class="form-row"><div class="input-wrapper warning-text"><img src="/images/app/icons/warning.png" class="warning-icon"> <i>You haven\'t created any FAQs for your website yet. Start by clicking "Add New Testimonial" on the right.</b></i><img src="/images/app/icons/help.png" class="help-icon help" data-subject="add-menus"></div></div>';   
            $rstr .= '</td></tr>';
        }
        
        return $rstr;
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function create() {
        
        $data = array(
            'faq_id' => '0',
            'faq_question' => '',
            'faq_answer' => ''
        );
        
        $rstr = '';
        $rstr .= '<div class="content-container">';
        $rstr .= $this->load->view('appForms/faqEditForm', $data, true);
        $rstr .= '</div>';
        
        return $rstr;   
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function edit() {
        $id = $this->uri->segment(5);
        
        $SQL = "SELECT * FROM ".$this->primary_table." WHERE faq_id = '".$id."' LIMIT 1";
        $query = $this->db->query($SQL);
        $row = $query->row();
        
        $data = array(
            'faq_id' => $row->faq_id,
            'faq_question' => $row->faq_question,
            'faq_answer' => $row->faq_answer
        );
        
        $rstr = '';
        $rstr .= '<div class="content-container">';
        $rstr .= $this->load->view('appForms/faqEditForm', $data, true);
        $rstr .= '</div>';
        
        return $rstr;   
    }

	// ----------------------------------- //
	// Saves and Creates pages
	// ----------------------------------- //
    public function save() {
        $id = $this->uri->segment(5);
                
        $data = array(
            'faq_id' => $this->input->post('faq_id'),
            'faq_question' => $this->input->post('faq_question'),
            'faq_answer' => $this->input->post('faq_answer')
        );
        
        
        if($id == 0) {          
            $this->db->insert($this->primary_table,$data);
			$return["id"] = $this->db->insert_id();
            $return["message"] = "The New FAQ [ ".$data["class_name"]." ] was created successfully.";
        } else {
            // Saving an existing Page    
            $this->db->where('faq_id',$id);
			$this->db->update($this->primary_table,$data); 
            $return["id"] = $id;
            $return["message"] = "Your changes have been saved.";
        }
        
        return $return;
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function getName($id) {
        $SQL = "SELECT * FROM ".$this->primary_table." WHERE faq_id = '".$id."'";
        $query = $this->db->query($SQL);
        $row = $query->row();
        
        return $row->faq_question;
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function editToolbox($id) {
        
        $toolboxArray = array();
		
        return $toolboxArray;
    }
    
    
    
    
}

?>
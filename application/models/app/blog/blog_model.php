<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog_model extends CI_Model {
    
    public $primary_table;
    public $path;
	
    
	// ----------------------------------- //
	// Initialize Our Primary Table
	// ----------------------------------- //
	public function initialize($table,$path) {
		$this->primary_table = $table;	
        $this->path = $path;
	}
	
    
	// ----------------------------------- //
	// Initialize Our Primary Table
	// ----------------------------------- //
    function renderBlogList() {
        $rstr = '';
        
        $rstr .= '<table class="fdcms-table" width="100%" cellpadding="0" cellspacing="0">';
        $rstr .= $this->renderTableRows();
        
        $rstr .= '<tr>';
        $rstr .= '<td colspan="2">';
        $rstr .= '<img src="/images/app/core/arrow_ltr.png"> <a href="javascript: void(0);" class="check-all edit-link">Check</a> / <a href="javascript: void(0);" class="uncheck-all delete-link">Uncheck</a> All';
        $rstr .= '<a href="javascript:void(0);" class="delete delete-selected" data-post="'.$this->path.'/delete">Delete Selected</a>';
        $rstr .= '</td>';
        $rstr .= '</tr>';
        
        $rstr .= '</table>';
        
        return $rstr;
    }

	// ----------------------------------- //
	// Row output for pages table
	// ----------------------------------- //
    public function renderTableRows() {
        $rstr = '';
        
        $this->db->from($this->primary_table);
        $query = $this->db->get();
        $count = $query->num_rows();
        $result = $query->result();
        
        if($count > 0) {
        
        foreach($result as $rs) {
            
            $class = ' class="native"';  
            
            $rstr .= '<tr id="'.$rs->blog_id.'">';
            
            // delete checkbox
            $rstr .= '<td width="20"'.$class.' valign="top">';
            $rstr .= '<input type="checkbox" name="'.$rs->blog_id.'" value="'.$rs->blog_id.'" id="box_'.$rs->blog_id.'" class="del-box" data-id="'.$rs->blog_id.'" data-post="'.$this->path.'/delete/'.$rs->blog_id.'">';
            $rstr .= '</td>';
            
            $rstr .= '<td'.$class.'>';
            $rstr .= '<div class="options-hover">';
            $rstr .= '<b>'.$rs->blog_title.'</b>';
            $rstr .= '<span class="row-options">';
            $rstr .= '<a href="'.$this->path.'/edit/'.$rs->blog_id.'" class="edit-link">Edit</a>';
            $rstr .= ' | <a href="javascript:void(0)" class="delete-link delete-item" data-id="'.$rs->blog_id.'" data-post="'.$this->path.'/delete/'.$rs->blog_id.'">Delete</a>';
            $rstr .= '</span>';
            $rstr .= '</div>';
            $rstr .= '</td>';
            
            $rstr .= '</tr>';  
        }
        
        } else {
            $rstr .= '<tr><td>';
            $rstr .= '<div class="form-row"><div class="input-wrapper warning-text"><img src="/images/app/icons/warning.png" class="warning-icon"> <i>You haven\'t created any Blog Entries for your website yet. Start by clicking "Create New Blog Entry" on the right.</b></i><img src="/images/app/icons/help.png" class="help-icon help" data-subject="add-menus"></div></div>';   
            $rstr .= '</td></tr>';
        }
        
        return $rstr;
    }
    
    

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function create() {
        
        $data = array(
            'blog_id' => '0',
            'blog_title' => 'My New Blog',
            'blog_content' => '',
            'blog_url' => '/my-new-blog',
            'blog_post_date' => '',
            'categories' => ''
        );
        
        $rstr = '';
        $rstr .= '<div class="content-container">';
        $rstr .= $this->load->view('appForms/blogEditForm', $data, true);
        $rstr .= '</div>';
        
        return $rstr;   
    }
    
    

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function renderCategorySelect() {
        $rstr = '';
        
        $rstr .= '<div class="form-row"><div class="input-wrapper"><label for="categories_selector">Categories</label>';
        $rstr .= $this->recurseCategorySelect();
        $rstr .= '</div></div>';
        return $rstr;   
    }
    
    

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function recurseCategorySelect($parent = 0,$level = 0) {
        $rstr = '';
        
        $this->db->where("category_parent",$parent);
        $this->db->from('fdcms_blog_categories');
        $query = $this->db->get();
        $result = $query->result();
        
        foreach($result as $rs) {
            
            $class = ' class="native"';
            if($level == 0) { $style = ' style="font-weight: 700;"'; } else { $style = ''; }
            
            $i = 1;
            $tabIn = '';
            while($i <= $level) {
                $tabIn .= '&mdash;'; 
                $i++;  
            }
            
            $optionStyle = ' style="margin-left: '.(($level * 12)+4).'px"';
            
            // delete checkbox
            $rstr .= '<div'.$class.$style.'>';
            $rstr .= '<input type="checkbox" class="category_box" name="'.$rs->category_id.'" value="'.$rs->category_id.'" id="category_'.$rs->category_id.'">';
            $rstr .= ' '.$tabIn.' '.$rs->category_name;
            $rstr .= '</div>';
            
            // Check for child pages
            $rstr .= $this->recurseCategorySelect($rs->category_id,($level+1));
             
        }
        
        return $rstr;
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function edit() {
        $id = $this->uri->segment(5);
        
        $SQL = "SELECT * FROM fdcms_blog_entries WHERE blog_id = '".$id."' LIMIT 1";
        $query = $this->db->query($SQL);
        $row = $query->row();
        
        $data = array(
            'blog_id' => $row->blog_id,
            'blog_title' => $row->blog_title,
            'blog_content' => $row->blog_content,
            'blog_url' => $row->blog_url
        );
        
        $SQL = "SELECT * FROM fdcms_blog_e2c WHERE blog_id = '".$id."'";
        $query = $this->db->query($SQL);
        $results = $query->result();
        
        $cat_arr = array();
        
        foreach($results as $rs) {
            $cat_arr[] = $rs->category_id;
        }
        
        $cat_list = implode(',',$cat_arr);
        $data['categories'] = $cat_list;
        
        $rstr = '';
        $rstr .= '<div class="content-container">';
        $rstr .= $this->load->view('appForms/blogEditForm', $data, true);
        $rstr .= '</div>';
        
        return $rstr;   
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function save() {
        $id = $this->uri->segment(5);
        $data = array(
            'blog_title' => $this->input->post('blog_title'),
            'blog_content' => $this->input->post('blog_content'),
            'blog_url' => $this->input->post('blog_url')
        );
        
        
        if($id == 0) {
            // Creating a new menu    
            $this->db->insert('fdcms_blog_entries',$data);
			$return["id"] = $this->db->insert_id();
            $return["message"] = "Your new Form [ ".$data["blog_title"]." ] was created successfully.";
        } else {
            // Update an existing menu
            $this->db->where('blog_id',$id);
			$this->db->update('fdcms_blog_entries',$data);           
            $return["id"] = $id;
            $return["message"] = "Your changes have been saved.";
        }
        
        // Remove all assignment associations
        $SQL = "DELETE FROM fdcms_blog_e2c WHERE blog_id = '".$id."'";
        $query = $this->db->query($SQL);
        
        foreach( explode( ',', $this->input->post('categories') ) as $category_id ) {
            $data = array(
                'category_id' => $category_id,
                'blog_id' => $return["id"]
            );
            $this->db->insert('fdcms_blog_e2c',$data);
        }
        
        return $return;
        
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function fields() {
        $id = $this->uri->segment(5);
        $data = array(
            'blog_id' => $id
        );
        $rstr = '';
        
        $rstr .= '<input type="hidden" name="menu_id" id="menu_id" value="'.$id.'">';
        
        $rstr .= '<div class="sortable-list" style="float: left;">';      
        $rstr .= '<h3>Add New Field</h3>';
        $rstr .= '<div class="sortable-menu">';
        $rstr .= $this->load->view('appForms/formFieldsForm',$data,true);
        $rstr .= '</div>';
        $rstr .= '<div class="menu-pages-add">';
        $rstr .= '<input type="button" value="Add Field" class="form-button-small trigger-add-field">';
        $rstr .= '</div>';
        $rstr .= '</div>';
        
        $rstr .= '<div class="sortable-list">';        
            $rstr .= '<h3>My Form</h3>';
            $rstr .= '<div class="sortable-menu">';
            $rstr .= '<ul id="form-items" class="sortable">';
            $rstr .= $this->renderCurrentFields($id);
            $rstr .= '</ul>';
            $rstr .= '</div>';
        $rstr .= '</div>';
        
        $rstr .= '<div class="clear"></div>';
        
        return $rstr;   
    }
    
    public function renderCurrentFields($id) {
        $SQL = "SELECT * FROM fdcms_forms_fields f JOIN fdcms_forms_f2f f2f ON (f2f.field_id = f.field_id) WHERE f2f.blog_id = '".$id."' ORDER BY sort_order ASC";
        $query = $this->db->query($SQL);
        $result = $query->result();
        
        $rstr = '';
        
        foreach($result as $rs) {
            $rstr .= '<li data-type="'.$rs->field_type.'" data-label="'.$rs->field_label.'" data-options="'.$rs->field_options.'" data-required="'.$rs->field_required.'"><div><span class="field-actions"><a href="javascript: void(0);" class="remove-item"><img src="/images/app/icons/remove-circle.png"></a></span><span class="field-required-'.$rs->field_required.'"></span><span class="field-label">'.$rs->field_label.'</span><br><span class="field-type small">'.$rs->field_type.'</span><span class="clear"></span></div></li>';
        }
        
        return $rstr;
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function getEntryName($id) {
        $SQL = "SELECT * FROM ".$this->primary_table." WHERE blog_id = '".$id."'";
        $query = $this->db->query($SQL);
        $row = $query->row();
        
        return $row->blog_title;
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function response() {
        $id = $this->uri->segment(5);
        
        $SQL = "SELECT blog_id,form_response_action,form_response_message,form_response_forward FROM fdcms_forms WHERE blog_id = '".$id."' LIMIT 1";
        $query = $this->db->query($SQL);
        $row = $query->row();
        
        $data = array(
            'blog_id' => $row->blog_id,
            'form_response_action' => $row->form_response_action,
            'form_response_message' => $row->form_response_message,
            'form_response_forward' => $row->form_response_forward
        );
        
        $rstr = '';
        $rstr .= '<div class="content-container">';
        $rstr .= $this->load->view('appForms/formResponseForm', $data, true);
        $rstr .= '</div>';
        
        return $rstr;   
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function saveresponse() {
        $id = $this->uri->segment(5);
        $data = array(
            'form_response_action' => $this->input->post('form_response_action'),
            'form_response_message' => $this->input->post('form_response_message'),
            'form_response_forward' => $this->input->post('form_response_forward')
        );
        
        // Update an existing menu
        $this->db->where('blog_id',$id);
        $this->db->update('fdcms_forms',$data);           
        $return["id"] = $id;
        $return["message"] = "Your changes have been saved.";
        
        return $return;
        
    }
    
    

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function editToolbox($id) {
        
        $toolboxArray = array();
        
        return $toolboxArray;
    }
    
}
?>
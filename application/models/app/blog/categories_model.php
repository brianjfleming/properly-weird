<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories_model extends CI_Model {
    
    public $primary_table;
    public $path;
	
    
	// ----------------------------------- //
	// Initialize Our Primary Table
	// ----------------------------------- //
	public function initialize($table,$path) {
		$this->primary_table = $table;	
        $this->path = $path;
	}
    
    public function renderTable() {
        $rstr = '';
        
        $rstr .= '<table class="fdcms-table" width="100%" cellpadding="0" cellspacing="0">';
        $rstr .= $this->renderTableRows();
        
        $rstr .= '<tr>';
        $rstr .= '<td colspan="2">';
        $rstr .= '<img src="/images/app/core/arrow_ltr.png"> <a href="javascript: void(0);" class="check-all edit-link">Check</a> / <a href="javascript: void(0);" class="uncheck-all delete-link">Uncheck</a> All';
        $rstr .= '<a href="javascript:void(0);" class="delete delete-selected" data-post="'.$this->path.'/delete">Delete Selected</a>';
        $rstr .= '</td>';
        $rstr .= '</tr>';
        
        $rstr .= '</table>';
        
        return $rstr;
    }

	// ----------------------------------- //
	// Row output for pages table
	// ----------------------------------- //
    public function renderTableRows($parent = 0,$level = 0) {
        $rstr = '';
        
        $this->db->where("category_parent",$parent);
        $this->db->from($this->primary_table);
        $query = $this->db->get();
        $result = $query->result();
        
        foreach($result as $rs) {
            
            $class = ' class="native"';
            if($level == 0) { $style = ' style="font-weight: 700;"'; } else { $style = ''; }
            
            $i = 1;
            $tabIn = '';
            while($i <= $level) {
                $tabIn .= '&mdash;'; 
                $i++;  
            }
            
            $optionStyle = ' style="margin-left: '.(($level * 12)+4).'px"';
            
            $rstr .= '<tr id="'.$rs->category_id.'">';
            
            // delete checkbox
            $rstr .= '<td width="20"'.$class.$style.' valign="top">';
            $rstr .= '<input type="checkbox" name="'.$rs->category_id.'" value="'.$rs->category_id.'" id="box_'.$rs->category_id.'" class="del-box" data-id="'.$rs->category_id.'" data-post="'.$this->path.'/delete/'.$rs->category_id.'">';
            $rstr .= '</td>';
            
            $rstr .= '<td'.$class.$style.'>';
            $rstr .= '<div class="options-hover">';
            $rstr .= $tabIn.' '.$rs->category_name;
            $rstr .= '<span class="row-options"'.$optionStyle.'>';
            $rstr .= '<a href="'.$this->path.'/edit/'.$rs->category_id.'" class="edit-link">Edit</a>';
            $rstr .= ' | <a href="'.$this->path.'/all/duplicate/'.$rs->category_id.'" class="duplicate-link duplicate-item">Duplicate</a>';
            $rstr .= ' | <a href="javascript:void(0)" class="delete-link delete-item" data-id="'.$rs->category_id.'" data-post="'.$this->path.'/delete/'.$rs->category_id.'">Delete</a>';
            $rstr .= '</span>';
            $rstr .= '</div>';
            $rstr .= '</td>';
            
            $rstr .= '</tr>';  
            
            // Check for child pages
            $rstr .= $this->renderTableRows($rs->category_id,($level+1));
             
        }
        
        return $rstr;
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function create() {
        
        $data = array(
            'category_id' => '0',
            'category_name' => 'My New Category',
            'category_slug' => '/my-new-category',
            'category_parent_select' => $this->renderCategorySelect(0, 0)
        );
        
        $rstr = '';
        $rstr .= '<div class="content-container">';
        $rstr .= $this->load->view('appForms/blogCategoryForm', $data, true);
        $rstr .= '</div>';
        
        return $rstr;   
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function edit() {
        $id = $this->uri->segment(5);
        
        $SQL = "SELECT * FROM fdcms_blog_categories WHERE category_id = '".$id."' LIMIT 1";
        $query = $this->db->query($SQL);
        $row = $query->row();
        
        $data = array(
            'category_id' => $row->category_id,
            'category_name' => $row->category_name,
            'category_slug' => $row->category_slug,
            'category_parent_select' => $this->renderCategorySelect($id, $row->category_parent)
        );
        
        $rstr = '';
        $rstr .= '<div class="content-container">';
        $rstr .= $this->load->view('appForms/blogCategoryForm', $data, true);
        $rstr .= '</div>';
        
        return $rstr;   
    }
	
	// ------------------------------------- //
	// Page | Add | Render parent select
	// ------------------------------------- //
	public function renderCategorySelect($id, $category_parent_id) {
		if($category_parent_id == '0') {
			$none_selected = "SELECTED";
		} else {
			$none_selected = "";	
		}
		
		$rstr = '';	
		$rstr .= '<select name="category_parent" id="category_parent">';
		$rstr .= '<option value="0" '.$none_selected.'>--NONE--</option>';		
		$rstr .= $this->recurseCategorySelect($category_parent_id, $id, 0, 0);
		$rstr .= '</select>';
		
		return $rstr;			
	}
	
	// ------------------------------------- //
	// Page | Add | Recurse parent select
	// ------------------------------------- //
	public function recurseCategorySelect($activeParent = 0, $id = 0, $parent = 0, $level = 0) {
		$rstr = '';
		$nextLevel = $level + 1;
		
		$SQL = "SELECT * FROM fdcms_blog_categories WHERE category_parent = '".$parent."' AND (category_id != '".$id."')";
		$query = $this->db->query($SQL);
				
		if($query->num_rows() > 0) {
			foreach($query->result() as $rs) {
				$pad = '';
				$i = 0;
				while($i<$level) {
					$pad .= '&mdash;';
					$i++;	
				}
				
				if($rs->category_id == $activeParent) {
					$selected = "SELECTED";	
				} else {
					$selected = "";
				}
				
				$rstr .= '<option value="'.$rs->category_id.'" '.$selected.'>';
				$rstr .= $pad.''.$rs->category_name;
				$rstr .= '</option>';
				
				$rstr .= $this->recurseCategorySelect($activeParent, $id, $rs->category_id, $nextLevel);
			}
		}
	
		return $rstr;
	}
    

	// ----------------------------------- //
	// Saves and Creates pages
	// ----------------------------------- //
    public function save() {
        $id = $this->uri->segment(5);
        
        $data = array(
            'category_id' => $this->input->post('category_id'),
            'category_name' => $this->input->post('category_name'),
            'category_slug' => $this->input->post('category_slug'),
            'category_parent' => $this->input->post('category_parent')
        );
        
        
        if($id == 0) {          
            $this->db->insert('fdcms_blog_categories',$data);
			$return["id"] = $this->db->insert_id();
            $return["message"] = "The New Blog Category [ ".$data["category_name"]." ] was created successfully.";
        } else {
            // Saving an existing Page    
            $this->db->where('category_id',$id);
			$this->db->update('fdcms_blog_categories',$data);
            $return["id"] = $id;
            $return["message"] = "Your changes have been saved.";
        }
        
        return $return;
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function getCategoryName($id) {
        $SQL = "SELECT * FROM fdcms_blog_categories WHERE category_id = '".$id."'";
        $query = $this->db->query($SQL);
        $row = $query->row();
        
        return $row->category_name;
    }
    
    
    
    
}

?>
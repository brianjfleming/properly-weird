<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {
    
    
    public function view_entry() {
        $url = $this->uri->segment(2);    
        
        $SQL = "SELECT * FROM fdcms_blog_entries WHERE blog_url = '/".$url."' LIMIT 1";
        $query = $this->db->query($SQL);
        $row = $query->row();
        
        $data["blog_id"] = $row->blog_id;
        $data["blog_title"] = $row->blog_title;
        $data["blog_url"] = $row->blog_url;
        $data["blog_content"] = $row->blog_content;
        $data["blog_post_date"] = $row->blog_post_date;
        
        $meta_title = $row->blog_title.' | Properly Weird';
        $meta_desc = '';
        
        $params = array('page_id' => '0', 'page_url' => '0','meta_title_override' => $meta_title,'meta_desc_override' => $meta_desc);
        $this->load->library("Fdcms",$params);
		$data["fdcms"] = $this->fdcms;
        $this->load->view('display/detail-blog',$data);
    }
    
    
}

?>
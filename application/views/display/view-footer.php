<footer class="footer row">
    <div class="wrapper">

        <div class="footer-left fifty">
            <span class="copyright">
                <?php $fdcms->copyright('Properly Weird'); ?>.<br />All Rights Reserved.
            </span>
            <span class="credit">
                <?php $fdcms->firm_copy(); ?>
            </span>
        </div>

        <div class="footer-right fifty">
            <div class="contact-stack">
                Tallulah - Metro Atlanta and <br /> surrounding areas.
                <span class="phone"><a href="tel:7705555555">770.555.5555</a></span>
            </div>
            <div class="contact-stack">
                Russell - Savannah, Charleston S.C., <br /> Greenville S.C., Nashville and beyond.
                <span class="phone"><a href="tel:7709999999">770.999.9999</a></span>
            </div>
        </div>

        <div class="clear"></div>
    </div>
</footer>

</div>
</body>
<?php
// Template Name: Homepage
?>

<!doctype html>
<html>
<head>
	<?php $this->load->view('display/view-head'); ?>
    <link rel="stylesheet" type="text/css" href="/css/display/homepage.min.css">
</head>

	<?php $this->load->view('display/view-header'); ?>

    <div class="hompage-hero">
        <?php $fdcms->render_slideshow(); ?>
    </div>

    <div class="homepage-featured">
        <div class="wrapper">
            <?php $fdcms->renderCTAs(); ?>
        </div>
    </div>

    <div class="homepage-cta">
        <div class="wrapper">
            <h2 class="pw-heading">Call, We Won't Bite</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin egestas elit in ligula tincidunt, in.</p>
            <a href="tel:770.555.5555" class="cta-button">770.555.5555</a>
        </div>
    </div>
    
	<?php $this->load->view('display/view-footer'); ?>

</html>
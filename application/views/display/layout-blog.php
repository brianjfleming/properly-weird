<?php
// Template Name: Blog List View
?>

<!doctype html>
<html>
<head>
    <?php $this->load->view('display/view-head'); ?>
    <link rel="stylesheet" type="text/css" href="/css/display/internal.css">
</head>

<?php $this->load->view('display/view-header'); ?>

<div class="internal-hero">
    <?php $fdcms->render_slideshow(); ?>
    <div class="internal-title">
        <div class="internal-title-table">
            <div class="internal-title-cell">
                <h1><?php $fdcms->the_title(); ?></h1>
            </div>
        </div>
    </div>
</div>

<div class="content-internal row">
    <div class="wrapper">

        <?php $fdcms->render_blog('blog'); ?>

    </div>
</div>

<div class="internal-cta row">
    <div class="wrapper">

        <?php $fdcms->html_block("CTA Content"); ?>

    </div>
</div>

<?php $this->load->view('display/view-footer'); ?>

</html>
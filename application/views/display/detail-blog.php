<!doctype html>
<html>
<head>
	<?php $this->load->view('display/view-head'); ?>
    <link rel="stylesheet" type="text/css" href="/css/display/internal.css">
</head>

	<?php $this->load->view('display/view-header'); ?>

    <div class="internal-hero">
        <div class="internal-title">
            <div class="internal-title-table">
                <div class="internal-title-cell">
                    <h1><?php echo $blog_title; ?></h1>
                </div>
            </div>
        </div>
    </div>
    
    <div class="content-internal row">
        <div class="wrapper">

            <?php echo $blog_content; ?>
        
        </div>
    </div>

    <div class="internal-cta row">
        <div class="wrapper">

            <h2>Heading for Blog Pages</h2>
            <p>Learn more about some kind of stuff</p>
            <a href="/blog">BACK TO BLOG</a>

        </div>
    </div>
    
	<?php $this->load->view('display/view-footer'); ?>

</html>
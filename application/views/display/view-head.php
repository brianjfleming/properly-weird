<meta charset="utf-8">
<title><?php $fdcms->meta_title(); ?></title>
<meta name="description" content="<?php $fdcms->meta_desc(); ?>">
<meta name="viewport" content="initial-scale=1">

<!-- REQUIRED by SYSTEM -->
<!-- ------------------ -->

	<!-- JS -->
	<script type="text/javascript" src="/js/system/jquery.2.1.1.min.js"></script>
	<script type="text/javascript" src="/js/display/master.js"></script>
	<script type="text/javascript" src="/js/display/jquery.dropdowns.js"></script>

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="/css/system/system.css">
	<link rel="stylesheet" type="text/css" href="/css/system/boilerplate.css">
	<link rel="stylesheet" type="text/css" href="/css/system/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/css/display/master.min.css">

	<!-- Favicon -->
	<link rel="icon" href="/images/display/pw-favicon.png" type="image/png"/>

<!-- ------------------ -->
<!--  END REQUIREMENTS  -->

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Barrio|Montserrat:300,400,600,700|Open+Sans:300,400,600,700" rel="stylesheet">

<!-- Dropdows -->
<script type="text/javascript">
	$(document).ready(function() {
		$('#mobile-trigger').click(function () {
			if($('ul.mobile-nav').is(':visible')) {
				$('div.page').removeClass('blur');
				$('div.mobile-screen').fadeOut(500);
				$('ul.mobile-nav').fadeOut(250);
			} else {
				$('div.page').addClass('blur');
				$('div.mobile-screen').fadeIn(250);
				$('ul.mobile-nav').fadeIn(500);
			}
		});
		$('div.mobile-screen').click(function () {
			$('div.page').removeClass('blur');
			$('div.mobile-screen').fadeOut(500);
			$('ul.mobile-nav').fadeOut(250);
		});
	});
</script>
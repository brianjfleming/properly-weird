<body>
<div class="page">

    <!-- Begin Header -->

    <header class="header row">
        <div class="header-logo left twenty">
            <a href="/"><img src="/images/display/pw-header-logo.png" /></a>
        </div>
        <div class="header-nav right eighty">
            <?php $fdcms->nav_menu('primary-nav'); ?>
        </div>
        <div class="clear"></div>
    </header>
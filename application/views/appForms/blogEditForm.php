<form id="pageForm" name="pageForm" method="post" action="/admin/blog/all/save/<?php echo $blog_id; ?>">
    <input type="hidden" name="blog_id" id="blog_id" value="<?php echo $blog_id; ?>">
    <input type="hidden" name="categories" id="categories" value="<?php echo $categories; ?>">

	<div class="form-row">
    <div class="input-wrapper">
    <label for="blog_title">Entry Name</label>
    <input type="text" name="blog_title" id="blog_title" value="<?php echo $blog_title; ?>" class="input-full" />
    </div>
    </div>
    
    <!-- --------------------- -->
    
    <div class="form-row">
    <div class="input-wrapper">
    <label for="blog_url_disabled">URL (Generated Automatically)</label>
    <input type="hidden" name="blog_url" id="blog_url" value="<?php echo $blog_url; ?>" />
    <input type="text" name="blog_url_disabled" id="blog_url_disabled" value="<?php echo $blog_url; ?>" disabled class="input-full disabled" />
    </div>
    </div>
    
    <!-- --------------------- -->

	<div class="form-row">
    <div class="mce-wrapper">
    <label for="blog_content">Entry Content</label>
    <textarea name="blog_content" id="blog_content" class="editor"><?php echo $blog_content; ?></textarea>
    </div>
    </div>
    
    <!-- --------------------- -->
</form>

<script type="text/javascript">

    $(window).load(function() {
        setupWYSIWYG();
        assignCategories();
        //createContent();
    });

    // Our typing timer etc and functions
    // for building our url to be awesome-sauce
    // Boosh, Kakow!
    var typingTimer;
    var doneTypingInterval = 800;
    
    $('#blog_title').keyup(function() {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(buildURL, doneTypingInterval);        
    });
    
    $('#blog_title').keydown(function() {
       clearTimeout(typingTimer); 
    });
    
    $('#blog_title').blur(function() {
       buildURL(); 
    });
    
    
    // This actually builds the url for us
    // only allows numbers letters and spaces
    // converts spaces to hyphens
    function buildURL() {
    var request = $('#blog_title').val();
    var rstr = request.replace(/[^a-zA-Z0-9 ]/g, "")
    var rstr = $.trim(rstr);
    var rstr = rstr.replace(/\s+/g, '-').toLowerCase();       
    $('#blog_url_disabled').val('/' + rstr);       
    }
    
    function setupWYSIWYG() {
        tinymce.init({ selector: ".editor", menubar: false, plugins: [ "advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste jbimages" ], toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages | fullscreen code", autosave_ask_before_unload: false, max_height: 450, min_height: 160, height : 180, resize : false, relative_urls: false });
    }
    
    // Convert data takes all of our dynamic content blocks and builds them into a jSon String
    // It's super-duper
    function convertData() {
        // Put our URL in our final
        $('#blog_url').val($('#blog_url_disabled').val());	
        
        // Get our Categories
        var assigned = getChecked();
        assigned = assigned.toString();
        console.log(assigned);
        $('#categories').val(assigned);  
	}
    
    function getChecked() {
        var sList = new Array();
        $('input[type=checkbox]').each(function () {
            if(this.checked) {
                var sThisVal = $(this).val();
                sList.push(sThisVal);
            }
        });
        return sList;
    }
    
    function assignCategories() {
        $('input[type=checkbox]').each(function() {
            var arr = '['+$('#categories').val()+']';
            var needle = $(this).val();
            console.log('Looking for '+needle+' in '+arr);
            console.log('   - '+$.inArray(needle,arr));
            if($.inArray(needle,arr) != -1) {
                console.log('   - found');
                $(this).attr('checked','checked');   
            } else {
                console.log('   - not found');
            }
        });
    }
</script>
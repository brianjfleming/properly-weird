<form id="pageForm" name="pageForm" method="post" action="/admin/extras/forms/savestyles/<?php echo $form_id; ?>">
    <input type="hidden" name="form_id" id="form_id" value="<?php echo $form_id; ?>">

	<div class="form-row">
    <div class="input-wrapper">
    <label for="form_logo_src">Logo URL <span class="small">(optional)</span></label>
    <input type="text" name="form_logo_src" id="form_logo_src" value="<?php echo $form_logo_src; ?>" class="input-full">
    </div>
    </div>
    
    <!-- --------------------- -->
    
	<div class="form-row">
    <div class="input-wrapper">
    <label for="form_site_title">Site Title <span class="small">(e.g. My Awesome Website)</span></label>
    <input type="text" name="form_site_title" id="form_site_title" value="<?php echo $form_site_title; ?>" class="input-full" />
    </div>
    </div>
    
    <!-- --------------------- -->

	<div class="form-row">
    <div class="input-wrapper">
    <label for="form_title_color">Title Color</label>
    <input type="text" name="form_title_color" id="form_title_color" value="<?php echo $form_title_color; ?>" class="input-full">
    </div>
    </div>
    
    <!-- --------------------- -->

	<div class="form-row">
    <div class="input-wrapper">
    <label for="form_title_background">Title Background Color</label>
    <input type="text" name="form_title_background" id="form_title_background" value="<?php echo $form_title_background; ?>" class="input-full">
    </div>
    </div>
    
    <!-- --------------------- -->

	<div class="form-row">
    <div class="input-wrapper">
    <label for="form_subtitle_color">Subtitle Color</label>
    <input type="text" name="form_subtitle_color" id="form_subtitle_color" value="<?php echo $form_subtitle_color; ?>" class="input-full">
    </div>
    </div>
    
    <!-- --------------------- -->

	<div class="form-row">
    <div class="input-wrapper">
    <label for="form_subtitle_background">Subtitle Background Color</label>
    <input type="text" name="form_subtitle_background" id="form_subtitle_background" value="<?php echo $form_subtitle_background; ?>" class="input-full">
    </div>
    </div>
    
    <!-- --------------------- -->
</form>
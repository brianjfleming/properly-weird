<form id="pageForm" name="pageForm" method="post" action="/admin/faqs/all/save/<?php echo $faq_id; ?>" enctype="multipart/form-data">
    <input type="hidden" name="faq_id" id="faq_id" value="<?php echo $faq_id; ?>">
	<div class="form-row">
    <div class="input-wrapper">
    <label for="faq_question">Question</label>
    <input type="text" name="faq_question" id="faq_question" value="<?php echo $faq_question; ?>" class="input-full" />
    </div>
    </div>
    
    <!-- --------------------- -->

	<div class="form-row">
    <div class="mce-wrapper">
    <label for="faq_answer">Answer</label>
    <textarea name="faq_answer" id="faq_answer" class="editor"><?php echo $faq_answer; ?></textarea>
    </div>
    </div>
    
    <!-- --------------------- -->
</form>

<script type="text/javascript">
    $(window).load(function() {
        setupWYSIWYG();
        //createContent();
    });
    
    function setupWYSIWYG() {
        tinymce.init({ selector: ".editor", menubar: false, plugins: [ "advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste" ], toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | fullscreen code", autosave_ask_before_unload: false, max_height: 350, min_height: 160, height : 180, resize : false, relative_urls: false });
    }
</script>
// JavaScript Document

$(document).ready(function() {

    $('.upload-cta').click(function() {
        var imageId = $(this).attr('data-id');
        var imageSrc = $(this).attr('data-img');

        var html = '<form action="/admin/pages/all/ctaimageupdate/1" method="post" enctype="multipart/form-data">';
        html += '<input type="hidden" name="cta_id" id="cta_id" value="'+imageId+'">';
        html += '<input type="hidden" name="image_src" id="image_src" value="'+imageSrc+'">';
        html += '<table><tr><td>';
        html += '<input type="file" name="file" id="file" value="" class="input-full" />';
        html += '</td><td>';
        html += '<input type="submit" value="Save Changes" class="form-button">';
        html += '</td></tr>';
        html += '</table>';
        html += '</form>';

        fdcmsAlert('Upload New Image',html);
    });
   
   $('.edit-cta').click(function() {
       var pageId = 1;
        var ctaId = $(this).attr('data-id');
        var ctaLink = $(this).attr('data-link');
        var ctaTitle = $(this).attr('data-title');
        var ctaText = $(this).attr('data-text');
        
        var html = '<form action="/admin/pages/all/ctaupdate/'+pageId+'" method="post" enctype="multipart/form-data">';
            html += '<input type="hidden" name="cta_id" id="cta_id" value="'+ctaId+'">';
            html += '<table>';
            html += '<tr>';
            html += '<td>';
            html += '<div class="form-row">';
            html += '<div class="input-wrapper" style="width: 250px;">';
            html += '<label for="image_title">Title</label>';
            html += '<input type="text" name="cta_title" id="cta_title" value="'+ctaTitle+'" class="input-full" />';
            html += '</div>';
            html += '</div>';
            html += '</td>';
            html += '</tr>';
           html += '<tr>';
           html += '<td>';
           html += '<div class="form-row">';
           html += '<div class="input-wrapper" style="width: 250px;">';
           html += '<label for="image_title">Text</label>';
           html += '<input type="text" name="cta_text" id="cta_text" value="'+ctaText+'" class="input-full" />';
           html += '</div>';
           html += '</div>';
           html += '</td>';
           html += '</tr>';
            html += '<tr>';
            html += '<td>';
            html += '<div class="form-row">';
            html += '<div class="input-wrapper" style="width: 250px;">';
            html += '<label for="image_link">Link</label>';
            html += '<input type="text" name="cta_link" id="cta_link" value="'+ctaLink+'" class="input-full" />';
            html += '</div>';
            html += '</div>';
            html += '</td>';
            html += '</tr>';
            html += '<tr>';
            html += '<td align="center">';
            html += '<input type="submit" value="Save Changes" class="form-button">';
            html += '</td>';
            html += '</tr>';
            html += '</table>';
            html += '</form>';
        
        
        fdcmsAlert('Edit Details',html);
   });
    
});
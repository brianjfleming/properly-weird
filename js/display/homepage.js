$(window).load( function() {
   showHeroText();
});


function showHeroText() {

    var hero_titles = $('span.homepage-hero-title-line');
    var time = 700;

    $('div.homepage-hero-title').slideDown(600);

    hero_titles.each( function() {
        var el = $(this);
        setTimeout( function() { el.css('opacity','1').css('left','0px'); }, time );
        time += 600;

    });

}

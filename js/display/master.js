/**
 * Created by brian on 7/28/16.
 */

$(document).ready(function() {
    // Open http:// links in a new tab
    $("a[href^='http://']").attr("target","_blank");
    $("a[href^='https://']").attr("target","_blank");

    $('a.menu-trigger').click(function () {
        if($('div.navigation-container').is(':visible')) {
            console.log('fadein');
            $('div.navigation-container').fadeOut(250);
        } else {
            $('div.navigation-container').fadeIn(500);
            console.log('fadeout');
        }
    });

});